/**
 * @file   Circle.h
 * @Author Ryszard Mikulec
 * @date   January, 2016
 * @brief  Class Circle header.
 */

#ifndef CIRCLE_H
#define CIRCLE_H

#include "Object.h"
#include "Vector4.h"
#include "Line.h"
#include "Matrix4.h"
#include <wx/dcbuffer.h>

class Circle : public Object {
public:
    /** @brief Circle constructor.
 *
 *  @param start_x coordinate x center of sphere
 *  @param start_y coordinate y center of sphere
 *  @param start_z coordinate z center of sphere
 *  @param diameter diameter of sphere
 *  @param line_width width of line
 */
    Circle(double start_x, double start_y, double start_z, double diameter, int line_width = 1)
        :_n(360), start_point(start_x, start_y, start_z), _diameter(diameter), _width(line_width){setPoints();}

    /** @brief Draw sphere on device context.
 *
 *  @param dc buffered device context where sphere is drawn
 *  @param transform matrix of transformation to display
 */
    void draw(wxBufferedDC *dc, Matrix4 transform) const;

    /** @brief Draw sphere as anaglyph on device context.
 *
 *  @param dc buffered device context where sphere is drawn
 *  @param transform matrix of transformation to display
 *  @param color array of two color in rgb where color[i] = r<<16 + g<<8 + b
 */  
  
    void drawAnaglyph(wxBufferedDC *dc, Matrix4 transform, int color[2]) const;
 
 /** @brief Circle destructor.
 *
 */
    ~Circle(){for( int i=0; i<3; i++) delete[] _point[i];}
private:
    void setPoints();
    Vector4 *_point[3];
    Vector4 start_point;
    double _diameter;
    int _n;
    int _width;
};

#endif /* CIRCLE_H */
