/**
 * @file   Line.h
 * @Author Ryszard Mikulec
 * @date   January, 2016
 * @brief  Class Line header.
 */

#ifndef LINE_H
#define LINE_H

#include "Object.h"
#include "Vector4.h"
#include "Matrix4.h"
#include <wx/dcbuffer.h>

class Line : public Object {
public:
    
    /** @brief Line defoult constructor.
 *
 */
    Line(){}
    
    /** @brief Line constructor.
 *
 *  @param start_x coordinate x of first point of the line
 *  @param start_y coordinate y of first point of the line
 *  @param start_z coordinate z of first point of the line
 *  @param end_x coordinate x of second point of the line
 *  @param end_y coordinate y of second of the line
 *  @param end_z coordinate z of second point of the line
 *  @param line_width width of line
 */
    Line(double start_x, double start_y, double start_z, double end_x, double end_y, double end_z, int line_width = 1)
        :start_point(start_x, start_y, start_z), end_point(end_x, end_y, end_z), _width(line_width){}
        
    /** @brief Line constructor.
 *
 *  @param start pointer to first point of the line
 *  @param end pointer to second point of the line
 *  @param line_width width of line
 */
    Line(Vector4 *start, Vector4 *end, int line_width = 1)
        :start_point(*start), end_point(*end), _width(line_width){}
        
    /** @brief Draw line on device context.
 *
 *  @param dc buffered device context where line is drawn
 *  @param transform matrix of transformation to display
 */
    void draw(wxBufferedDC *dc, Matrix4 transform) const;
    
    /** @brief Draw line as anaglyph on device context.
 *
 *  @param dc buffered device context where line is drawn
 *  @param transform matrix of transformation to display
 *  @param color array of two color in rgb where color[i] = r<<16 + g<<8 + b
 */  
    void drawAnaglyph(wxBufferedDC *dc, Matrix4 transform, int color[2]) const;
private:
    Vector4 start_point;
    Vector4 end_point;
    int _width;
    const static double d;
};

#endif /* LINE_H */
