#include "Line.h"

const double Line::d = -2.5;

void Line::draw(wxBufferedDC *dc, Matrix4 transform) const{
    int w, h;
    dc->GetSize(&w, &h);
    Vector4 tmp_start_point = transform * start_point;        //new
    Vector4 tmp_end_point = transform * end_point;        //new
    
    tmp_start_point.Set(tmp_start_point.GetD1()*(-d)/(tmp_start_point.GetD3()-d), tmp_start_point.GetD2()*(-d)/(tmp_start_point.GetD3()-d), 0);
    tmp_end_point.Set(tmp_end_point.GetD1()*(-d)/(tmp_end_point.GetD3()-d), tmp_end_point.GetD2()*(-d)/(tmp_end_point.GetD3()-d), 0);
    dc->SetPen(wxPen(wxColour(0, 0, 0), _width));
    dc->DrawLine(wxPoint(tmp_start_point.GetD1()*w/h*2*100, tmp_start_point.GetD2()*w/h*2*100), wxPoint(tmp_end_point.GetD1()*w/h*2*100, tmp_end_point.GetD2()*w/h*2*100));
}

void Line::drawAnaglyph(wxBufferedDC *dc, Matrix4 transform, int color[2]) const{
    int w, h;
    Vector4 tmp_start_point = transform * start_point;        //new
    Vector4 tmp_end_point = transform * end_point;        //new
    dc->GetSize(&w, &h);
    
    Matrix4 translate_mat_left;
    
    translate_mat_left.setElement(0, 0, 1.0);
    translate_mat_left.setElement(1, 1, 1.0);
    translate_mat_left.setElement(2, 2, 1.0);
    translate_mat_left.setElement(3, 3, 1.0);
    translate_mat_left.setElement(0, 3, -0.01);
    translate_mat_left.setElement(1, 3, 0.00);
    translate_mat_left.setElement(2, 3, 0.00);
    
    Matrix4 translate_mat_right;
    
    translate_mat_right.setElement(0, 0, 1.0);
    translate_mat_right.setElement(1, 1, 1.0);
    translate_mat_right.setElement(2, 2, 1.0);
    translate_mat_right.setElement(3, 3, 1.0);
    translate_mat_right.setElement(0, 3, 0.2);
    translate_mat_right.setElement(1, 3, 0.00);
    translate_mat_right.setElement(2, 3, 0.00);
    
    Matrix4 translate_mat_reverse;
    
    translate_mat_reverse.setElement(0, 0, 1.0);
    translate_mat_reverse.setElement(1, 1, 1.0);
    translate_mat_reverse.setElement(2, 2, 1.0);
    translate_mat_reverse.setElement(3, 3, 1.0);
    translate_mat_reverse.setElement(0, 3, -0.19);
    translate_mat_reverse.setElement(1, 3, 0.00);
    translate_mat_reverse.setElement(2, 3, 0.00);
    
    
    Vector4 tmp_start_point_t, tmp_end_point_t;
    Vector4 tmp_start_point_c(tmp_start_point), tmp_end_point_c(tmp_end_point);
 
    
    //commented lines below are for testing purposes
    //
    /*
    Matrix4 mat;
    mat.setElement(0, 0, 1.0);
    mat.setElement(1, 1, 1.0);
    mat.setElement(2, 2, 1.0);
    mat.setElement(3, 3, 1.0);
    mat.setElement(0, 3, 1.5);
    mat.setElement(1, 3, 0);
    mat.setElement(2, 3, 0);
    Vector4 tmp_start_point2(tmp_start_point), tmp_end_point2(tmp_end_point);
    tmp_start_point2.Set(tmp_start_point2.GetD1()*(-d)/(tmp_start_point2.GetD3()-d), tmp_start_point2.GetD2()*(-d)/(tmp_start_point2.GetD3()-d), 0);
    tmp_end_point2.Set(tmp_end_point2.GetD1()*(-d)/(tmp_end_point2.GetD3()-d), tmp_end_point2.GetD2()*(-d)/(tmp_end_point2.GetD3()-d), 0);
    tmp_start_point2 = mat*tmp_start_point2;
    tmp_end_point2 = mat*tmp_end_point2;
    dc->SetPen(wxPen(wxColour(0, 0, 0), _width));
    dc->DrawLine(wxPoint(tmp_start_point2.GetD1()*w/h*2*100, tmp_start_point2.GetD2()*w/h*2*100), wxPoint(tmp_end_point2.GetD1()*w/h*2*100, tmp_end_point2.GetD2()*w/h*2*100));
    */
    
    tmp_start_point = translate_mat_left*tmp_start_point;
    tmp_end_point = translate_mat_left*tmp_end_point;
    
    tmp_start_point_t = translate_mat_right*tmp_start_point_c;
    tmp_end_point_t = translate_mat_right*tmp_end_point_c;
    
    tmp_start_point.Set(tmp_start_point.GetD1()*(-d)/(tmp_start_point.GetD3()-d), tmp_start_point.GetD2()*(-d)/(tmp_start_point.GetD3()-d), 0);
    tmp_end_point.Set(tmp_end_point.GetD1()*(-d)/(tmp_end_point.GetD3()-d), tmp_end_point.GetD2()*(-d)/(tmp_end_point.GetD3()-d), 0);
    
    tmp_start_point_t.Set(tmp_start_point_t.GetD1()*(-d)/(tmp_start_point_t.GetD3()-d), tmp_start_point_t.GetD2()*(-d)/(tmp_start_point_t.GetD3()-d), 0);
    tmp_end_point_t.Set(tmp_end_point_t.GetD1()*(-d)/(tmp_end_point_t.GetD3()-d), tmp_end_point_t.GetD2()*(-d)/(tmp_end_point_t.GetD3()-d), 0);
    
    tmp_start_point_t = translate_mat_reverse*tmp_start_point_t;
    tmp_end_point_t = translate_mat_reverse*tmp_end_point_t;
    
    dc->SetPen(wxPen(wxColour(color[1] & 255, (color[1] & 255 << 8)  >> 8, (color[1] & 255 << 16) >> 16, 0), _width));
    dc->DrawLine(wxPoint(tmp_start_point_t.GetD1()*w/h*2*100, tmp_start_point_t.GetD2()*w/h*2*100), wxPoint(tmp_end_point_t.GetD1()*w/h*2*100, tmp_end_point_t.GetD2()*w/h*2*100));
    dc->SetPen(wxPen(wxColour(color[0] & 255, (color[0] & 255 << 8)  >> 8, (color[0] & 255 << 16) >> 16, 0), _width));
    dc->DrawLine(wxPoint(tmp_start_point.GetD1()*w/h*2*100, tmp_start_point.GetD2()*w/h*2*100), wxPoint(tmp_end_point.GetD1()*w/h*2*100, tmp_end_point.GetD2()*w/h*2*100));
}
