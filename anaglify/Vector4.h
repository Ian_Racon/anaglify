/* 
 * File:   Vector4.h
 * Author: ian
 *
 * Created on December 27, 2015, 1:42 PM
 */

#ifndef VECTOR4_H
#define VECTOR4_H

class Vector4 {
public:
    /** @brief Get x coordinate of vector.
 * @return x coordinate of vector
 */
    double GetD1() const { return data[0];}
    
    /** @brief Get y coordinate of vector.
 * @return y coordinate of vector
 */
    double GetD2() const { return data[1];}
    
    /** @brief Get z coordinate of vector.
 * @return z coordinate of vector
 */
    double GetD3() const { return data[2];}
    
    /** @brief Get fourth coordinate of vector.
 * @return fourth coordinate of vector
 */
    double GetD4() const { return data[3];}
    
    /** @brief Set coordinates of vector.
 * @param d1 x coordinate of vector
 * @param d2 y coordinate of vector
 * @param d3 z coordinate of vector
 * @param d4 fourth coordinate of vector
 */
    void Set(double const d1, double const d2, double const d3);
    
    /** @brief Set fourth coordinate of vector.
 * @param d4 fourth coordinate of vector
 */
    void SetDim4(double const d4);
    
    /** @brief Vector4 constructor.
 *
 *  @param d1 coordinate x of vector
 *  @param d2 coordinate y of vector
 *  @param d3 coordinate z of vector
 */
    Vector4(double const d1, double const d2, double const d3);
    
    /** @brief Vector4 defoult constructor.
 *
 */
    Vector4();
    
    /** @brief Vector4 copy constructor.
 *
 */
    Vector4(const Vector4& orig);
    
    /** @brief Vector4 operator[] to get coordinate of vector.
 *
 */
    double & operator[](int el) { return data[el];}
    
    /** @brief Vector4 destructor.
 *
 */
    virtual ~Vector4();
    
private:
    double data[4];
};

#endif /* VECTOR4_H */

