/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Matrix4.cpp
 * Author: ian
 * 
 * Created on December 27, 2015, 2:47 PM
 */

#include "Matrix4.h"

Matrix4::Matrix4() {
    this->data[0][0]=0.0; this->data[0][1]=0.0; this->data[0][2]=0.0; this->data[0][3]=0.0;
    this->data[1][0]=0.0; this->data[1][1]=0.0; this->data[1][2]=0.0; this->data[1][3]=0.0;
    this->data[2][0]=0.0; this->data[2][1]=0.0; this->data[2][2]=0.0; this->data[2][3]=0.0;
    this->data[3][0]=0.0; this->data[3][1]=0.0; this->data[3][2]=0.0; this->data[3][3]=1.0;
}

Matrix4 
Matrix4::operator* (const Matrix4 gMatrix)
{
 int i,j,k;
 Matrix4 tmp;

 for (i=0;i<4;i++)
    for (j=0;j<4;j++)
     {
        tmp.setElement(i, j, 0.0);
        for (k=0;k<4;k++) tmp.setElement(i, j, tmp.getElement(i, j)+(data[i][k]*gMatrix.getElement(k, j)));
     }
 return tmp;
}

Matrix4::Matrix4(const Matrix4& orig) {
    this->data[0][0]=orig.getElement(0,0); this->data[0][1]=orig.getElement(0,1); this->data[0][2]=orig.getElement(0,2); this->data[0][3]=orig.getElement(0,3);
    this->data[1][0]=orig.getElement(1,0); this->data[1][1]=orig.getElement(1,1); this->data[1][2]=orig.getElement(1,2); this->data[1][3]=orig.getElement(1,3);
    this->data[2][0]=orig.getElement(2,0); this->data[2][1]=orig.getElement(2,1); this->data[2][2]=orig.getElement(2,2); this->data[2][3]=orig.getElement(2,3);
    this->data[3][0]=orig.getElement(3,0); this->data[3][1]=orig.getElement(3,1); this->data[3][2]=orig.getElement(3,2); this->data[3][3]=orig.getElement(3,3);
}

Matrix4::~Matrix4() {
}

Vector4 operator* (const Matrix4 gMatrix, Vector4 gVector)
{
 unsigned int i,j;
 Vector4 tmp;

 for (i=0;i<4;i++)
  {
   tmp[i]=0.0;
   for (j=0;j<4;j++) tmp[i]=tmp[i]+(gMatrix.getElement(i, j)*gVector[j]);
  }
 return tmp;
}
