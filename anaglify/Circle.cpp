#include "Circle.h"   

void Circle::setPoints(){
    Matrix4 xrot_mat;
    Matrix4 yrot_mat;
    Matrix4 zrot_mat;
    Matrix4 trans_mat;
    
    xrot_mat.setElement(0, 0, 1);
    xrot_mat.setElement(3, 3, 1);

    yrot_mat.setElement(1, 1, 1);
    yrot_mat.setElement(3, 3, 1);

    
    zrot_mat.setElement(2, 2, 1);
    zrot_mat.setElement(3, 3, 1);
    
    trans_mat.setElement(0, 0, 1.0);
    trans_mat.setElement(1, 1, 1.0);
    trans_mat.setElement(2, 2, 1.0);
    trans_mat.setElement(3, 3, 1.0);
    trans_mat.setElement(0, 3, start_point.GetD1());
    trans_mat.setElement(1, 3, start_point.GetD2());
    trans_mat.setElement(2, 3, start_point.GetD3());
    
    for( int i=0; i<3; i++)
    {
        _point[i] = new Vector4[_n];
        for( int j=0; j<_n; j++)
            if(i == 0)
            {
                _point[i][j] = Vector4(0, _diameter/2, 0);
                xrot_mat.setElement(1, 1, cos(j*M_PI/180));
                xrot_mat.setElement(1, 2, -sin(j*M_PI/180));
                xrot_mat.setElement(2, 1, sin(j*M_PI/180));
                xrot_mat.setElement(2, 2, cos(j*M_PI/180));
                _point[i][j] = trans_mat*xrot_mat*_point[i][j];
            }
            else if(i == 1)
            {
                _point[i][j] = Vector4(0, 0, _diameter/2);
                yrot_mat.setElement(0, 0, cos(j*M_PI/180));     
                yrot_mat.setElement(0, 2, sin(j*M_PI/180));
                yrot_mat.setElement(2, 0, -sin(j*M_PI/180));
                yrot_mat.setElement(2, 2, cos(j*M_PI/180));
                _point[i][j] = trans_mat*yrot_mat*_point[i][j];
            }
            else if(i == 2)
            {
                _point[i][j] = Vector4(_diameter/2, 0, 0);
                zrot_mat.setElement(0, 0, cos(j*M_PI/180));    
                zrot_mat.setElement(0, 1, -sin(j*M_PI/180));
                zrot_mat.setElement(1, 0, sin(j*M_PI/180));
                zrot_mat.setElement(1, 1, cos(j*M_PI/180));
                _point[i][j] = trans_mat*zrot_mat*_point[i][j];
            }
    }
}

void Circle::draw(wxBufferedDC *dc, Matrix4 transform) const{
    Line line;
    for( int i=0; i<3; i++)
        for( int j=1; j<_n; j++)
        {
            line = Line(&_point[i][j-1], &_point[i][j], _width);
            line.draw(dc, transform);    
        }
}

void Circle::drawAnaglyph(wxBufferedDC *dc, Matrix4 transform, int color[2]) const{
    Line line;
    for( int i=0; i<3; i++)
        for( int j=1; j<_n; j++)
        {
            line = Line(&_point[i][j-1], &_point[i][j], _width);
            line.drawAnaglyph(dc, transform, color);    
        }
}
