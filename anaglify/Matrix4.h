/* 
 * File:   Matrix4.h
 * Author: ian
 *
 * Created on December 27, 2015, 2:47 PM
 */
#include "Vector4.h"

#ifndef MATRIX4_H
#define MATRIX4_H

class Matrix4 {
public:
    /** @brief Matrix4 defoult constructor.
 *
 */
    Matrix4();
    
    /** @brief Matrix4 copy constructor.
 *
 */
    Matrix4(const Matrix4& orig);
    
    /** @brief Matrix4 operator* to multiple 2 matrixes.
 *
 */
    Matrix4 operator*(const Matrix4);
    
    /** @brief Set element's value on given position.
 * @param i column number
 * @param j row number
 * @param value value to set
 */
    void setElement(int i, int j, double value) {data[i][j] = value;}

    /** @brief Matrix4 operator* to multiple matrix and vector.
 *
 */
    friend Vector4 operator*(const Matrix4,const Vector4);
    
    /** @brief Get element's value on given position.
 * @param i column number
 * @param j row number
 */
    double getElement (int i, int j) const { return data[i][j];}
    
    /** @brief Matrix4 destructor.
 *
 */
    virtual ~Matrix4();
private:
    double data[4][4];
};

#endif /* MATRIX4_H */

