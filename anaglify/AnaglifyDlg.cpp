///-----------------------------------------------------------------
///
/// @file      AnaglifyDlg.cpp
/// @author    Michal
/// Created:   2015-11-27 11:27:00
/// @section   DESCRIPTION
///            AnaglifyDlg class implementation
///
///------------------------------------------------------------------

#include "AnaglifyDlg.h"
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include "Vector4.h"
#include <wx/dcbuffer.h>
#include "Object.h"
#include "Circle.h"
#include "Line.h"

std::vector <Object*> objects; //new
extern double d;
int color[2];

//Do not add custom headers
//wxDev-C++ designer will remove them
////Header Include Start
////Header Include End

//----------------------------------------------------------------------------
// AnaglifyDlg
//----------------------------------------------------------------------------
//Add Custom Events only in the appropriate block.
//Code added in other places will be removed by wxDev-C++
////Event Table Start
BEGIN_EVENT_TABLE(AnaglifyDlg,wxDialog)
	////Manual Code Start
	////Manual Code End
	
    EVT_CLOSE(AnaglifyDlg::OnClose)
	EVT_CHECKBOX(ID_WXCHECKBOX_RES,AnaglifyDlg::WxCheckBoxResolutionClick)
	EVT_TEXT(ID_WXEDIT_HEIGHT,AnaglifyDlg::WxEditHeightUpdated)
	
	EVT_TEXT(ID_WXEDIT_WIDTH,AnaglifyDlg::WxEditWidthUpdated)
	EVT_BUTTON(ID_WXBUTTONSAVE,AnaglifyDlg::WxButtonSaveClick)
	EVT_RADIOBUTTON(ID_WXRADIOBUTTONGB,AnaglifyDlg::WxRadioButtonGBClick)
	EVT_RADIOBUTTON(ID_WXRADIOBUTTON_RG,AnaglifyDlg::WxRadioButtonRGClick)
	EVT_RADIOBUTTON(ID_WXRADIOBUTTON_RB,AnaglifyDlg::WxRadioButtonRBClick)
	EVT_CHECKBOX(ID_WXCHECKBOX1,AnaglifyDlg::WxCheckBox3DClick)
	
	EVT_COMMAND_SCROLL(ID_WXSCROLLBAR_ZOOM,AnaglifyDlg::WxScrollBarZoomScroll)
	
	EVT_COMMAND_SCROLL(ID_WXSCROLLBAR_Z,AnaglifyDlg::WxScrollBar_ZScroll)
	
	EVT_COMMAND_SCROLL(ID_WXSCROLLBAR_Y,AnaglifyDlg::WxScrollBar_YScroll)
	
	EVT_COMMAND_SCROLL(ID_WXSCROLLBAR_X,AnaglifyDlg::WxScrollBar_XScroll)
	EVT_BUTTON(ID_WXBUTTONLOAD,AnaglifyDlg::WxButtonLoadClick)
	
	EVT_UPDATE_UI(ID_WXPANEL1,AnaglifyDlg::WxPanel1UpdateUI)
END_EVENT_TABLE()
////Event Table End

AnaglifyDlg::AnaglifyDlg(wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &position, const wxSize& size, long style)
: wxDialog(parent, id, title, position, size, style)
{
	CreateGUIControls();
}

AnaglifyDlg::~AnaglifyDlg()
{
} 

void AnaglifyDlg::CreateGUIControls()
{
	//Do not add custom code between
	//GUI Items Creation Start and GUI Items Creation End.
	//wxDev-C++ designer will remove them.
	//Add the custom code before or after the blocks
	////GUI Items Creation Start

	WxBoxSizer1 = new wxBoxSizer(wxHORIZONTAL);
	this->SetSizer(WxBoxSizer1);
	this->SetAutoLayout(true);

	WxPanel = new wxPanel(this, ID_WXPANEL1, wxPoint(5, 166), wxSize(349, 41));
	WxBoxSizer1->Add(WxPanel, 1, wxEXPAND | wxALL, 5);

	WxBoxSizer2 = new wxBoxSizer(wxVERTICAL);
	WxBoxSizer1->Add(WxBoxSizer2, 0, wxALIGN_CENTER | wxALL, 5);

	WxButtonLoad = new wxButton(this, ID_WXBUTTONLOAD, _("Wczytaj obrazek"), wxPoint(51, 5), wxSize(138, 25), 0, wxDefaultValidator, _("WxButtonLoad"));
	WxBoxSizer2->Add(WxButtonLoad, 0, wxALIGN_CENTER | wxALL, 5);

	WxStaticText1 = new wxStaticText(this, ID_WXSTATICTEXT1, _("Obroty"), wxPoint(73, 40), wxDefaultSize, 0, _("WxStaticText1"));
	WxStaticText1->SetFont(wxFont(11, wxSWISS, wxNORMAL, wxBOLD, false, _("Segoe UI Semibold")));
	WxBoxSizer2->Add(WxStaticText1, 0, wxALIGN_CENTER | wxALL, 5);

	WxBoxSizer3 = new wxBoxSizer(wxHORIZONTAL);
	WxBoxSizer2->Add(WxBoxSizer3, 0, wxALIGN_CENTER | wxALL, 5);

	WxStaticText2 = new wxStaticText(this, ID_WXSTATICTEXT2, _("Os X"), wxPoint(5, 5), wxDefaultSize, 0, _("WxStaticText2"));
	WxStaticText2->SetForegroundColour(wxColour(128,0,0));
	WxBoxSizer3->Add(WxStaticText2, 0, wxALIGN_CENTER | wxALL, 5);

	WxScrollBar_X = new wxScrollBar(this, ID_WXSCROLLBAR_X, wxPoint(43, 6), wxSize(121, 17), wxSB_HORIZONTAL, wxDefaultValidator, _("WxScrollBar_X"));
	WxScrollBar_X->Enable(false);
	WxBoxSizer3->Add(WxScrollBar_X, 0, wxALIGN_CENTER | wxALL, 5);

	WxStaticText_X = new wxStaticText(this, ID_WXSTATICTEXT_X, _("0"), wxPoint(174, 5), wxDefaultSize, 0, _("WxStaticText_X"));
	WxBoxSizer3->Add(WxStaticText_X, 0, wxALIGN_CENTER | wxALL, 5);

	WxBoxSizer4 = new wxBoxSizer(wxHORIZONTAL);
	WxBoxSizer2->Add(WxBoxSizer4, 0, wxALIGN_CENTER | wxALL, 5);

	WxStaticText3 = new wxStaticText(this, ID_WXSTATICTEXT3, _("Os Y"), wxPoint(5, 5), wxDefaultSize, 0, _("WxStaticText3"));
	WxStaticText3->SetForegroundColour(wxColour(128,0,0));
	WxBoxSizer4->Add(WxStaticText3, 0, wxALIGN_CENTER | wxALL, 5);

	WxScrollBar_Y = new wxScrollBar(this, ID_WXSCROLLBAR_Y, wxPoint(43, 6), wxSize(121, 17), wxSB_HORIZONTAL, wxDefaultValidator, _("WxScrollBar_Y"));
	WxScrollBar_Y->Enable(false);
	WxBoxSizer4->Add(WxScrollBar_Y, 0, wxALIGN_CENTER | wxALL, 5);

	WxStaticText_Y = new wxStaticText(this, ID_WXSTATICTEXT_Y, _("0"), wxPoint(174, 5), wxDefaultSize, 0, _("WxStaticText_Y"));
	WxBoxSizer4->Add(WxStaticText_Y, 0, wxALIGN_CENTER | wxALL, 5);

	WxBoxSizer5 = new wxBoxSizer(wxHORIZONTAL);
	WxBoxSizer2->Add(WxBoxSizer5, 0, wxALIGN_CENTER | wxALL, 5);

	WxStaticText4 = new wxStaticText(this, ID_WXSTATICTEXT4, _("Os Z"), wxPoint(5, 5), wxDefaultSize, 0, _("WxStaticText4"));
	WxStaticText4->SetForegroundColour(wxColour(128,0,0));
	WxBoxSizer5->Add(WxStaticText4, 0, wxALIGN_CENTER | wxALL, 5);

	WxScrollBar_Z = new wxScrollBar(this, ID_WXSCROLLBAR_Z, wxPoint(43, 6), wxSize(121, 17), wxSB_HORIZONTAL, wxDefaultValidator, _("WxScrollBar_Z"));
	WxScrollBar_Z->Enable(false);
	WxBoxSizer5->Add(WxScrollBar_Z, 0, wxALIGN_CENTER | wxALL, 5);

	WxStaticText_Z = new wxStaticText(this, ID_WXSTATICTEXT_Z, _("0"), wxPoint(174, 5), wxDefaultSize, 0, _("WxStaticText_Z"));
	WxBoxSizer5->Add(WxStaticText_Z, 0, wxALIGN_CENTER | wxALL, 5);
	
	WxStaticText6 = new wxStaticText(this, ID_WXSTATICTEXT6, _("Przyblizanie/oddalanie"), wxPoint(46, 191), wxDefaultSize, 0, _("WxStaticText6"));
	WxStaticText6->SetFont(wxFont(11, wxSWISS, wxNORMAL, wxBOLD, false, _("Segoe UI Semibold")));
	WxBoxSizer2->Add(WxStaticText6, 0, wxALIGN_BOTTOM | wxALIGN_CENTER | wxALL, 5);

	WxBoxSizer6 = new wxBoxSizer(wxHORIZONTAL);
	WxBoxSizer2->Add(WxBoxSizer6, 0, wxALIGN_CENTER | wxALL, 5);

	WxScrollBarZoom = new wxScrollBar(this, ID_WXSCROLLBAR_ZOOM, wxPoint(5, 6), wxSize(121, 17), wxSB_HORIZONTAL, wxDefaultValidator, _("WxScrollBarZoom"));
	WxScrollBarZoom->Enable(false);
	WxBoxSizer6->Add(WxScrollBarZoom, 0, wxALIGN_CENTER | wxALL, 5);

	WxStaticTextZoom = new wxStaticText(this, ID_WXSTATICTEXT_ZOOM, _("1 x"), wxPoint(136, 5), wxDefaultSize, 0, _("WxStaticTextZoom"));
	WxBoxSizer6->Add(WxStaticTextZoom, 0, wxALIGN_CENTER | wxALL, 5);

	WxCheckBox3D = new wxCheckBox(this, ID_WXCHECKBOX1, _("Tryb 3D"), wxPoint(65, 191), wxSize(68, 17), 0, wxDefaultValidator, _("WxCheckBox3D"));
	WxCheckBox3D->SetForegroundColour(wxColour(0,0,128));
	WxCheckBox3D->SetFont(wxFont(10, wxSWISS, wxNORMAL, wxBOLD, false, _("Segoe UI Semibold")));
	WxBoxSizer2->Add(WxCheckBox3D, 0, wxALIGN_CENTER | wxALL, 5);

	WxStaticText5 = new wxStaticText(this, ID_WXSTATICTEXT5, _("Rodzaj okularow 3D:"), wxPoint(27, 218), wxDefaultSize, 0, _("WxStaticText5"));
	WxStaticText5->SetFont(wxFont(11, wxSWISS, wxNORMAL, wxBOLD, false, _("Segoe UI Semibold")));
	WxBoxSizer2->Add(WxStaticText5, 0, wxALIGN_CENTER | wxALL, 5);

	WxRadioButtonRB = new wxRadioButton(this, ID_WXRADIOBUTTON_RB, _("Czerwono - niebieskie"), wxPoint(29, 252), wxSize(170, 17), 0, wxDefaultValidator, _("WxRadioButtonRB"));
	WxRadioButtonRB->Enable(false);
	WxBoxSizer2->Add(WxRadioButtonRB, 0, wxALIGN_CENTER | wxALL, 5);

	WxRadioButtonRG = new wxRadioButton(this, ID_WXRADIOBUTTON_RG, _("Czerwono - zielone"), wxPoint(29, 279), wxSize(170, 17), 0, wxDefaultValidator, _("WxRadioButtonRG"));
	WxRadioButtonRG->Enable(false);
	WxBoxSizer2->Add(WxRadioButtonRG, 0, wxALIGN_CENTER | wxALL, 5);

	WxRadioButtonGB = new wxRadioButton(this, ID_WXRADIOBUTTONGB, _("Czerwono - cyjanowe"), wxPoint(29, 306), wxSize(170, 17), 0, wxDefaultValidator, _("WxRadioButtonGB"));
	WxRadioButtonGB->Enable(false);
	WxBoxSizer2->Add(WxRadioButtonGB, 0, wxALIGN_CENTER | wxALL, 5);

	WxButtonSave = new wxButton(this, ID_WXBUTTONSAVE, _("Zapisz obrazek"), wxPoint(49, 333), wxSize(115, 25), 0, wxDefaultValidator, _("WxButtonSave"));
	WxBoxSizer2->Add(WxButtonSave, 0, wxALIGN_CENTER | wxALL, 5);

	WxStaticText7 = new wxStaticText(this, ID_WXSTATICTEXT7, _("Rozdzielczosc zapisu:"), wxPoint(61, 441), wxDefaultSize, 0, _("WxStaticText7"));
	WxStaticText7->SetFont(wxFont(10, wxSWISS, wxNORMAL, wxBOLD, false, _("Segoe UI Semibold")));
	WxBoxSizer2->Add(WxStaticText7, 0, wxALIGN_CENTER | wxALL, 5);

	WxBoxSizer7 = new wxBoxSizer(wxHORIZONTAL);
	WxBoxSizer2->Add(WxBoxSizer7, 0, wxALIGN_CENTER | wxALL, 5);

	WxEditWidth = new wxTextCtrl(this, ID_WXEDIT_WIDTH, _("800"), wxPoint(5, 5), wxSize(37, 19), 0, wxDefaultValidator, _("WxEditWidth"));
	WxEditWidth->SetMaxLength(4);
	WxEditWidth->Enable(false);
	WxBoxSizer7->Add(WxEditWidth, 0, wxALIGN_CENTER | wxALL, 5);

	WxStaticText8 = new wxStaticText(this, ID_WXSTATICTEXT8, _("x"), wxPoint(52, 5), wxDefaultSize, 0, _("WxStaticText8"));
	WxBoxSizer7->Add(WxStaticText8, 0, wxALIGN_CENTER | wxALL, 5);

	WxEditHeight = new wxTextCtrl(this, ID_WXEDIT_HEIGHT, _("600"), wxPoint(71, 5), wxSize(37, 19), 0, wxDefaultValidator, _("WxEditHeight"));
	WxEditHeight->SetMaxLength(4);
    WxEditHeight->Enable(false);
	WxBoxSizer7->Add(WxEditHeight, 0, wxALIGN_CENTER | wxALL, 5);
	
	WxCheckBoxResolution = new wxCheckBox(this, ID_WXCHECKBOX_RES, _("Domyslna"), wxPoint(118, 6), wxSize(74, 17), 0, wxDefaultValidator, _("WxCheckBoxResolution"));
	WxCheckBoxResolution->SetValue(true);
	WxBoxSizer7->Add(WxCheckBoxResolution, 0, wxALIGN_CENTER | wxALL, 5);
        
    WxOpenFileDialog =  new wxFileDialog(this, wxT("Choose a file"), wxT(""), wxT(""), wxT("*.*"), wxFD_OPEN);
    WxSaveFileDialog =  new wxFileDialog(this, _("Choose a file"), _(""), _(""), _("*.*"), wxFD_SAVE);
        
	SetTitle(_("Anaglify"));
	SetIcon(wxNullIcon);
	
	Layout();
	GetSizer()->Fit(this);
	GetSizer()->SetSizeHints(this);
	Center();
	
	////GUI Items Creation End
	WxScrollBar_X->SetScrollbar(0, 1, 360, 1,true);
	WxScrollBar_X->Enable(true);
	
	WxScrollBar_Y->SetScrollbar(0, 1, 360, 1,true);
	WxScrollBar_Y->Enable(true);
	
	WxScrollBar_Z->SetScrollbar(0, 1, 360, 1,true);
	WxScrollBar_Z->Enable(true);
	
	WxScrollBarZoom->SetScrollbar(99,1,200,1,true);
	WxScrollBarZoom->Enable(true);
	
	WxSaveFileDialog->SetWildcard("PNG (*.png)|*.png");
	img->AddHandler(new wxPNGHandler);
}

void AnaglifyDlg::OnClose(wxCloseEvent& /*event*/)
{
	Destroy();
}

/*
 * WxPanel1UpdateUI
 */
void AnaglifyDlg::WxPanel1UpdateUI(wxUpdateUIEvent& event)
{
    Repaint();
}

/*
 * Repaint
 */
void AnaglifyDlg::Repaint()
{
    double xrot_val = (WxScrollBar_X->GetThumbPosition()); 
    double yrot_val = (WxScrollBar_Y->GetThumbPosition()); 
    double zrot_val = (WxScrollBar_Z->GetThumbPosition());
    double scale_val = (WxScrollBarZoom->GetThumbPosition());
    Matrix4 xrot_mat;
    Matrix4 yrot_mat;
    Matrix4 zrot_mat;
    Matrix4 scale_mat;
    
    xrot_mat.setElement(0, 0, 1);
    xrot_mat.setElement(1, 1, cos(xrot_val*M_PI/180));
    xrot_mat.setElement(1, 2, -sin(xrot_val*M_PI/180));
    xrot_mat.setElement(2, 1, sin(xrot_val*M_PI/180));
    xrot_mat.setElement(2, 2, cos(xrot_val*M_PI/180));
    xrot_mat.setElement(3, 3, 1);

    yrot_mat.setElement(0, 0, cos(yrot_val*M_PI/180));     
    yrot_mat.setElement(0, 2, sin(yrot_val*M_PI/180));
    yrot_mat.setElement(1, 1, 1);
    yrot_mat.setElement(2, 0, -sin(yrot_val*M_PI/180));
    yrot_mat.setElement(2, 2, cos(yrot_val*M_PI/180));
    yrot_mat.setElement(3, 3, 1);

    zrot_mat.setElement(0, 0, cos(zrot_val*M_PI/180));    
    zrot_mat.setElement(0, 1, -sin(zrot_val*M_PI/180));
    zrot_mat.setElement(1, 0, sin(zrot_val*M_PI/180));
    zrot_mat.setElement(1, 1, cos(zrot_val*M_PI/180));
    zrot_mat.setElement(2, 2, 1);
    zrot_mat.setElement(3, 3, 1);
    
    scale_mat.setElement(0,0,scale_val/100.0);
    scale_mat.setElement(1,1,scale_val/100.0);
    scale_mat.setElement(2,2,scale_val/100.0);
    
    Vector4 tmp_start_point;
    Vector4 tmp_end_point;
    
    int w,h;
    WxPanel->GetSize(&w,&h);
    bitmap = new wxBitmap(w,h);
    wxClientDC dcx(WxPanel);
    wxBufferedDC dc(&dcx,*bitmap);
    dc.SetBackground(wxBrush(wxColour(255,255,255)));
    dc.Clear();
    dc.SetDeviceOrigin(w/2,h/2);
    
    Matrix4 transform = xrot_mat*yrot_mat*zrot_mat*scale_mat; //new
    
    for(int i=0; i< objects.size(); ++i)//new
        if (WxCheckBox3D->IsChecked())
            objects[i]->drawAnaglyph(&dc, transform, color); //new
        else
            objects[i]->draw(&dc, transform); //new
}

/*
 * WxButtonLoadClick
 */
void AnaglifyDlg::WxButtonLoadClick(wxCommandEvent& event)
{
    if(WxOpenFileDialog->ShowModal()==wxID_OK)
    {
        std::string str; //new
    	double x1, y1, z1, x2, y2, z2;
	    int type, width;

    	std::ifstream in(WxOpenFileDialog->GetPath().mb_str());
		if (in.is_open()){
			objects.clear(); //new
			while(!in.eof()){
				std::getline(in, str); //new
				std::istringstream iss(str); //new
                iss>>type>>x1>>y1>>z1>>x2>>y2>>z2>>width;
                if  (type==1) //new
				    objects.push_back(new Line(x1, y1, z1, x2, y2, z2, width)); //new
				else if (type == 2) //new
    				objects.push_back(new Circle(x1, y1, z1, x2, (int)y2)); //new
            }
            in.close();
		}
    }
}

/*
 * WxScrollBar_XScroll
 */
void AnaglifyDlg::WxScrollBar_XScroll(wxScrollEvent& event)
{
	wxString t;
    t<<WxScrollBar_X->GetThumbPosition();
	WxStaticText_X->SetLabel(t);
	Repaint();
}

/*
 * WxScrollBar_YScroll
 */
void AnaglifyDlg::WxScrollBar_YScroll(wxScrollEvent& event)
{
	wxString t;
    t<<WxScrollBar_Y->GetThumbPosition();
	WxStaticText_Y->SetLabel(t);
	Repaint();
}

/*
 * WxScrollBar_ZScroll
 */
void AnaglifyDlg::WxScrollBar_ZScroll(wxScrollEvent& event)
{
	wxString t;
    t<<WxScrollBar_Z->GetThumbPosition();
	WxStaticText_Z->SetLabel(t);
	Repaint();
}



/*
 * WxCheckBox3DClick
 */
void AnaglifyDlg::WxCheckBox3DClick(wxCommandEvent& event)
{
    if (WxCheckBox3D->IsChecked())
    {
            color[0] = 255 | 0 << 8 | 0 << 16;
            color[1] = 0 | 0 << 8 | 255 << 16;
	    WxRadioButtonRB->Enable(true);
	    WxRadioButtonRG->Enable(true);
	    WxRadioButtonGB->Enable(true);
    }
	else
    {
            WxRadioButtonRB->Enable(false);
	    WxRadioButtonRG->Enable(false);
	    WxRadioButtonGB->Enable(false);   
    }
	   
}

/*
 * WxRadioButton1Click
 */
void AnaglifyDlg::WxRadioButtonRBClick(wxCommandEvent& event)
{
    color[0] = 255 | 0 << 8 | 0 << 16;
    color[1] = 0 | 0 << 8 | 255 << 16;
}

/*
 * WxRadioButtonRGClick
 */
void AnaglifyDlg::WxRadioButtonRGClick(wxCommandEvent& event)
{
    color[0] = 255 | 0 << 8 | 0 << 16;
    color[1] = 0 | 255 << 8 | 0 << 16;
}

/*
 * WxButtonSaveClick
 */
void AnaglifyDlg::WxButtonSaveClick(wxCommandEvent& event)
{
	if (WxSaveFileDialog->ShowModal() == wxID_OK)
	{
        wxImage image = bitmap->ConvertToImage();
        
        if (WxCheckBoxResolution->IsChecked())
            image.SaveFile(WxSaveFileDialog->GetPath(), wxBITMAP_TYPE_PNG);
        else
        {
            int width = wxAtoi(WxEditWidth->GetLineText(0));
            int height = wxAtoi(WxEditHeight->GetLineText(0));
            image.Rescale(width,height);
            image.SaveFile(WxSaveFileDialog->GetPath(), wxBITMAP_TYPE_PNG);
        }    
    }
	  
}

/*
 * WxRadioButtonGBClick
 * Red-cyan
 */
void AnaglifyDlg::WxRadioButtonGBClick(wxCommandEvent& event)
{
    color[0] = 255 | 0 << 8 | 0 << 16;
    color[1] = 0 | 255 << 8 | 255 << 16;
}

/*
 * WxScrollBarZoomScroll
 */
void AnaglifyDlg::WxScrollBarZoomScroll(wxScrollEvent& event)
{
	wxString t;
    t<<(WxScrollBarZoom->GetThumbPosition()+1.0)/100.0<<" x";
	WxStaticTextZoom->SetLabel(t);
	Repaint();
}

/*
 * WxEditWidthUpdated
 */
void AnaglifyDlg::WxEditWidthUpdated(wxCommandEvent& event)
{
	// insert your code here
}

/*
 * WxEditHeightUpdated
 */
void AnaglifyDlg::WxEditHeightUpdated(wxCommandEvent& event)
{
	// insert your code here
}

void AnaglifyDlg::WxCheckBoxResolutionClick(wxCommandEvent& event)
{
    if (WxCheckBoxResolution->IsChecked())
    {
	    WxEditWidth->Enable(false);
	    WxEditHeight->Enable(false);
    }
	else
    {
        WxEditWidth->Enable(true);
	    WxEditHeight->Enable(true); 
    }
}
