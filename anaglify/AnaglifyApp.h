//---------------------------------------------------------------------------
//
// Name:        AnaglifyApp.h
// Author:      Michal
// Created:     2015-11-27 11:27:00
// Description: 
//
//---------------------------------------------------------------------------

#ifndef __ANAGLIFYDLGApp_h__
#define __ANAGLIFYDLGApp_h__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

class AnaglifyDlgApp : public wxApp
{
	public:
		bool OnInit();
		int OnExit();
};

#endif
