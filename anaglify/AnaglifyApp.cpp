//---------------------------------------------------------------------------
//
// Name:        AnaglifyApp.cpp
// Author:      Michal
// Created:     2015-11-27 11:27:00
// Description: 
//
//---------------------------------------------------------------------------

#include "AnaglifyApp.h"
#include "AnaglifyDlg.h"

IMPLEMENT_APP(AnaglifyDlgApp)

bool AnaglifyDlgApp::OnInit()
{
	AnaglifyDlg* dialog = new AnaglifyDlg(NULL);
	SetTopWindow(dialog);
	dialog->Show(true);		
	return true;
}
 
int AnaglifyDlgApp::OnExit()
{
	return 0;
}
