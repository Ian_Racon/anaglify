///-----------------------------------------------------------------
///
/// @file      AnaglifyDlg.h
/// @author    Michal
/// Created:   2015-11-27 11:27:00
/// @section   DESCRIPTION
///            AnaglifyDlg class declaration
///
///------------------------------------------------------------------

#ifndef __ANAGLIFYDLG_H__
#define __ANAGLIFYDLG_H__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
	#include <wx/dialog.h>
#else
	#include <wx/wxprec.h>
#endif

//Do not add custom headers between 
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/radiobut.h>
#include <wx/checkbox.h>
#include <wx/scrolbar.h>
#include <wx/stattext.h>
#include <wx/button.h>
#include <wx/panel.h>
#include <wx/sizer.h>
#include <wx/filedlg.h>
////Header Include End

#include "Vector4.h"
#include "Matrix4.h"
#include <vector>

////Dialog Style Start
#undef AnaglifyDlg_STYLE
#define AnaglifyDlg_STYLE wxCAPTION | wxRESIZE_BORDER | wxDIALOG_NO_PARENT | wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxCLOSE_BOX
////Dialog Style End

class AnaglifyDlg : public wxDialog
{
	private:
		DECLARE_EVENT_TABLE();
		
	public:
		AnaglifyDlg(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("Anaglify"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = AnaglifyDlg_STYLE);
		virtual ~AnaglifyDlg();
		void WxPanel1UpdateUI(wxUpdateUIEvent& event);
		void Repaint();
		void WxButtonLoadClick(wxCommandEvent& event);
		void WxScrollBar1Scroll(wxScrollEvent& event);
		void WxScrollBar_YScroll(wxScrollEvent& event);
		void WxScrollBar_XScroll(wxScrollEvent& event);
		void WxScrollBar_YScroll0(wxScrollEvent& event);
		void WxScrollBar_ZScroll(wxScrollEvent& event);
		void WxCheckBox3DClick(wxCommandEvent& event);
		void WxRadioButtonRBClick(wxCommandEvent& event);
		void WxRadioButtonRGClick(wxCommandEvent& event);
		void WxButtonSaveClick(wxCommandEvent& event);
		void WxRadioButtonGBClick(wxCommandEvent& event);
		void WxScrollBarZoomScroll(wxScrollEvent& event);
		void WxEditWidthUpdated(wxCommandEvent& event);
		void WxEditHeightUpdated(wxCommandEvent& event);
		void WxCheckBoxResolutionClick(wxCommandEvent& event);
	
	private:
		//Do not add custom control declarations between 
		//GUI Control Declaration Start and GUI Control Declaration End.
		//wxDev-C++ will remove them. Add custom code after the block.
		////GUI Control Declaration Start
		wxCheckBox *WxCheckBoxResolution;
        wxTextCtrl *WxEditHeight;
		wxStaticText *WxStaticText8;
		wxTextCtrl *WxEditWidth;
		wxBoxSizer *WxBoxSizer7;
		wxStaticText *WxStaticText7;
		wxButton *WxButtonSave;
		wxRadioButton *WxRadioButtonGB;
		wxRadioButton *WxRadioButtonRG;
		wxRadioButton *WxRadioButtonRB;
		wxStaticText *WxStaticText5;
		wxCheckBox *WxCheckBox3D;
		wxStaticText *WxStaticTextZoom;
		wxScrollBar *WxScrollBarZoom;
		wxBoxSizer *WxBoxSizer6;
		wxStaticText *WxStaticText6;
		wxStaticText *WxStaticText_Z;
		wxScrollBar *WxScrollBar_Z;
		wxStaticText *WxStaticText4;
		wxBoxSizer *WxBoxSizer5;
		wxStaticText *WxStaticText_Y;
		wxScrollBar *WxScrollBar_Y;
		wxStaticText *WxStaticText3;
		wxBoxSizer *WxBoxSizer4;
		wxStaticText *WxStaticText_X;
		wxScrollBar *WxScrollBar_X;
		wxStaticText *WxStaticText2;
		wxBoxSizer *WxBoxSizer3;
		wxStaticText *WxStaticText1;
		wxButton *WxButtonLoad;
		wxBoxSizer *WxBoxSizer2;
		wxPanel *WxPanel;
		wxBoxSizer *WxBoxSizer1;
        wxFileDialog *WxOpenFileDialog;
        wxFileDialog *WxSaveFileDialog;
		////GUI Control Declaration End
		wxBitmap *bitmap;
		wxImage *img;
		
	private:
		//Note: if you receive any error with these enum IDs, then you need to
		//change your old form code that are based on the #define control IDs.
		//#defines may replace a numeric value for the enum names.
		//Try copy and pasting the below block in your old form header files.
		enum
		{
			////GUI Enum Control ID Start
			ID_WXCHECKBOX_RES = 1043,
	     	ID_WXEDIT_HEIGHT = 1042,
			ID_WXSTATICTEXT8 = 1041,
			ID_WXEDIT_WIDTH = 1040,
			ID_WXSTATICTEXT7 = 1037,
			ID_WXBUTTONSAVE = 1027,
			ID_WXRADIOBUTTONGB = 1026,
			ID_WXRADIOBUTTON_RG = 1025,
			ID_WXRADIOBUTTON_RB = 1024,
			ID_WXSTATICTEXT5 = 1023,
			ID_WXCHECKBOX1 = 1022,
			ID_WXSTATICTEXT_ZOOM = 1033,
			ID_WXSCROLLBAR_ZOOM = 1032,
			ID_WXSTATICTEXT6 = 1029,
			ID_WXSTATICTEXT_Z = 1021,
			ID_WXSCROLLBAR_Z = 1020,
			ID_WXSTATICTEXT4 = 1019,
			ID_WXSTATICTEXT_Y = 1017,
			ID_WXSCROLLBAR_Y = 1016,
			ID_WXSTATICTEXT3 = 1015,
			ID_WXSTATICTEXT_X = 1013,
			ID_WXSCROLLBAR_X = 1012,
			ID_WXSTATICTEXT2 = 1010,
			ID_WXSTATICTEXT1 = 1008,
			ID_WXBUTTONLOAD = 1006,
			ID_WXPANEL1 = 1004,
			////GUI Enum Control ID End
			ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
		};
                
	private:
		void OnClose(wxCloseEvent& event);
		void CreateGUIControls();
};

#endif
