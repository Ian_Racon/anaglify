/* 
 * File:   Vector4.cpp
 * Author: ian
 * 
 * Created on December 27, 2015, 1:42 PM
 */

#include "Vector4.h"

Vector4::Vector4() {
    this->data[0] = 0.0;
    this->data[1] = 0.0;
    this->data[2] = 0.0;
    this->data[3] = 1.0;
}

Vector4::Vector4(double const d1, double const d2, double const d3) {
    this->data[0] = d1;
    this->data[1] = d2;
    this->data[2] = d3;
    this->data[3] = 1.0;
}

Vector4::Vector4(const Vector4& orig) {
    this->data[0] = orig.GetD1();
    this->data[1] = orig.GetD2();
    this->data[2] = orig.GetD3();
    this->data[3] = orig.GetD4();
}

void
Vector4::Set(const double d1, const double d2, const double d3){
    this->data[0] = d1;
    this->data[1] = d2;
    this->data[2] = d3;
    this->data[3] = 1.0;
}

void
Vector4::SetDim4(const double d4){
    this->data[3] = d4;
}

Vector4::~Vector4() {
}

