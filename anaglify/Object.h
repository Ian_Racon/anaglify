/**
 * @file   Object.h
 * @Author Ryszard Mikulec
 * @date   January, 2016
 * @brief  Class Object header
 */
 
#ifndef OBJECT_H
#define OBJECT_H

#include "Vector4.h"
#include "Matrix4.h"
#include <wx/dcbuffer.h>

class Object {
public:
    
    /** @brief Draw object on device context.
 *
 *  @param dc buffered device context where object is drawn
 *  @param transform matrix of transformation to display
 */
    virtual void draw(wxBufferedDC *dc, Matrix4 transform) const = 0;
    
    /** @brief Draw object as anaglyph on device context.
 *
 *  @param dc buffered device context where object is drawn
 *  @param transform matrix of transformation to display
 *  @param color array of two color in rgb where color[i] = r<<16 + g<<8 + b
 */  
    virtual void drawAnaglyph(wxBufferedDC *dc, Matrix4 transform, int color[2]) const = 0;
};

#endif /* OBJECT_H */
